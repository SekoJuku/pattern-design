package Factory;

public class Ship implements Transport{

    @Override
    public void load() {
        System.out.println("It's loaded!");
    }

    @Override
    public void drive() {
        System.out.println("It's coming to destination!");
    }
}
