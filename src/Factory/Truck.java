package Factory;

public class Truck implements Transport {

    @Override
    public void load() {
        System.out.println("It's loaded now!");
    }

    @Override
    public void drive() {
        System.out.println("It's gone!");
    }
}
