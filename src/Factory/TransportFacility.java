package Factory;

public interface TransportFacility {
    public Transport createTransport(String type);
}
