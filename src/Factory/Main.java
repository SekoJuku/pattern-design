package Factory;

public class Main {
    public static void main(String args[]) {
        CreateTransport ct = new CreateTransport();
        Transport truck = ct.createTransport("Land");
        truck.load();
        truck.drive();

        Transport ship = ct.createTransport("Sea");
        ship.load();
        ship.drive();

    }
}
