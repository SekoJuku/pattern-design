package Factory;

public class CreateTransport implements TransportFacility{
    Transport transport;
    @Override
    public Transport createTransport(String type) {
        if(type.equalsIgnoreCase("Sea")) {
            return new Ship();
        }
        if(type.equalsIgnoreCase("Land")) {
            return new Truck();
        }
        return null;
    }
}
