package Adapter;

public class MusicianAdapter implements Musician{
    MusicianType musicianType;

    public MusicianAdapter(String theme) {
        if(theme.equalsIgnoreCase("Classic")) {
            musicianType = new ClassicMusician();
        }
        else if(theme.equalsIgnoreCase("Pop")){
            musicianType = new NotClassicMusician();
        }
    }

    @java.lang.Override
    public void sing(String theme, String name) {
        if(theme.equalsIgnoreCase("Classic")) {
            musicianType.singClassic(name);
        }
        else if(theme.equalsIgnoreCase("Pop")) {
            musicianType.singNotClassic(name);
        }
    }
}
