package Adapter;

public class Main {
    public static void main(String args[]) {
        MusicianImplementation a = new MusicianImplementation();

        a.sing("Classic","Symphony No5");
        a.sing("Classic","Fur Felise");
        a.sing("Pop","Old Town Road");
        a.sing("K-Pop","DDU-DDU-DU-DU");
    }
}
