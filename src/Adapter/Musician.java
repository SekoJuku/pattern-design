package Adapter;

public interface Musician {
    public void sing(String theme,String name);
}