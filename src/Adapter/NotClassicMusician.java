package Adapter;

public class NotClassicMusician implements MusicianType{
    @java.lang.Override
    public void singClassic(String name) { }

    @java.lang.Override
    public void singNotClassic(String name) {
        System.out.println("Musician sang " + name +"!\n");
    }
}
