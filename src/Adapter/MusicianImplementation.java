package Adapter;

public class MusicianImplementation implements Musician {
    MusicianAdapter musicianAdapter;

    @java.lang.Override
    public void sing(String theme, String name) {
        if(theme.equalsIgnoreCase("Classic") || theme.equalsIgnoreCase("Pop")) {
            musicianAdapter = new MusicianAdapter(theme);
            musicianAdapter.sing(theme, name);
        }
        else {
            System.out.println("He can't play music other than classic, pop!\n");
        }
    }
}
