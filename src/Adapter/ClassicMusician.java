package Adapter;

public class ClassicMusician implements MusicianType{

    @java.lang.Override
    public void singClassic(String name) {
        System.out.println("Musician sang " + name + "!\n");
    }

    @java.lang.Override
    public void singNotClassic(String name) { }
}
