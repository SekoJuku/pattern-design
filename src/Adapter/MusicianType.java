package Adapter;

public interface MusicianType {
    public void singClassic(String name);
    public void singNotClassic(String name);
}
